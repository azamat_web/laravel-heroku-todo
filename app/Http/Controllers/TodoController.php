<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use App\Todo;
use Illuminate\Support\Facades\Validator;

class TodoController extends Controller
{

    /**
     * @return Todo[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {
        return Todo::orderBy('updated_at', 'desc')->get();
    }

    public function show($id)
    {
        $todo = Todo::find($id);
        return $todo;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:255',
            'description' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->getMessageBag(), 404);
        }

        try {
            $todo = new Todo;
            $todo->title = $request->title;
            $todo->description = $request->description;
            $todo->save();

            return response()->json('is created success', 201);
        } catch (\Exception $e) {
            return response()->json('no content', 204);
        }

    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:255',
            'description' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->getMessageBag(), 404);
        }

        try {
            $todo = Todo::find($request->id);
            $todo->title = $request->title;
            $todo->description = $request->description;
            $todo->save();

            return response()->json('is update success', 201);
        } catch (\Exception $e) {
            return response()->json('data not found', 404);
        }

    }

    public function delete(Request $request)
    {
        try {
            $todo = Todo::find($request->id);
            if (!$todo)
                return response()->json('data not found', 404);

            $todo->delete();
            return response()->json('is deleted success', 204);
            
        } catch (\Exception $e) {
            return response()->json('error');
        }
    }

}

